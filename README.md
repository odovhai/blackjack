This application allows to play in traditional shoe Blackjack. 
In `application.yml` you can configure the next properties

    | property               | Description                        | Default value |
    | -----------------------| ---------------------------------------------------|
    | blackjack.deck-count   | Number of card decks for shoe      | 4             |
    | blackjack.seed-capital | Initial user amount of money       | 100           |
    | blackjack.max-balance  | Max money allowed to be registered | 10000         |
    | blackjack.bet          | Initial user bet                   | 10            |
    
The app is written with Java 13 with it's several features related to switch-cases and teen collectors. 
You need to run the application with enabling preview to have it working:  
    
`$ java --enable-preview -jar blackjack-0.0.1-SNAPSHOT.jar userName`

Where `userName` is a program argument.

If you use Intellij Idea make sure java 13 with preview enabled as well in:

`Project Settings/Modules/Language level: 13 (Preview)`

You may also need to install Lombok Plugin for you IDE and enable annotation processing.

If you still have some maven build troubles with JDK13 there was committed the latest jar build into `${project.root}/tmp/blackjack-0.0.1-SNAPSHOT.jar`
