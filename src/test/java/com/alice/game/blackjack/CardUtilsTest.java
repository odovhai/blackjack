package com.alice.game.blackjack;

import com.alice.game.blackjack.model.dto.Card;
import com.alice.game.blackjack.model.dto.CardSuite;
import com.alice.game.blackjack.model.dto.CardValue;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.alice.game.blackjack.util.CardUtils.*;
import static org.junit.Assert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class CardUtilsTest {

    @Test
    public void blackjackDeckTest() {
        int expectedDeckSize = 52;
        List<Card> actualDeck = createShuffledDeckOfCards();
        Assert.assertThat(actualDeck.size(), Is.is(expectedDeckSize));

        Set<Card> expectedCards = new HashSet<>();
        for (CardValue value : CardValue.values()) {
            for (CardSuite suite : CardSuite.values()) {
                expectedCards.add(Card.of(value, suite));
            }
        }

        if (expectedCards.size() != expectedDeckSize) {
            throw new IllegalStateException(String.format("Initialized card set is invalid, check model of [%s] and [%s].", CardSuite.class.getName(), CardValue.class.getName()));
        }

        actualDeck.forEach(actualCard -> Assert.assertTrue(expectedCards.contains(actualCard)));
    }

    @Test
    public void getNoAceScoreTest() {
        assertThat(getNoAceScore(CardValue.V2), Is.is(2));
        assertThat(getNoAceScore(CardValue.V3), Is.is(3));
        assertThat(getNoAceScore(CardValue.V4), Is.is(4));
        assertThat(getNoAceScore(CardValue.V5), Is.is(5));
        assertThat(getNoAceScore(CardValue.V6), Is.is(6));
        assertThat(getNoAceScore(CardValue.V7), Is.is(7));
        assertThat(getNoAceScore(CardValue.V8), Is.is(8));
        assertThat(getNoAceScore(CardValue.V9), Is.is(9));
        assertThat(getNoAceScore(CardValue.V10), Is.is(10));
        assertThat(getNoAceScore(CardValue.JACK), Is.is(10));
        assertThat(getNoAceScore(CardValue.QUEEN), Is.is(10));
        assertThat(getNoAceScore(CardValue.KING), Is.is(10));
        assertThat(getNoAceScore(CardValue.ACE), Is.is(0));
    }

    @Test
    public void calculateAcesScoreTest() {

        for (int softScore = 2; softScore < 8; softScore++) {
            assertThat(calculateAcesScore(softScore, 0), Is.is(0));
            assertThat(calculateAcesScore(softScore, 1), Is.is(11));
            assertThat(calculateAcesScore(softScore, 2), Is.is(12));
            assertThat(calculateAcesScore(softScore, 3), Is.is(13));
            assertThat(calculateAcesScore(softScore, 4), Is.is(14));
        }

        assertThat(calculateAcesScore(8, 0), Is.is(0));
        assertThat(calculateAcesScore(8, 1), Is.is(11));
        assertThat(calculateAcesScore(8, 2), Is.is(12));
        assertThat(calculateAcesScore(8, 3), Is.is(13));
        assertThat(calculateAcesScore(8, 4), Is.is(4));
        assertThat(calculateAcesScore(9, 0), Is.is(0));
        assertThat(calculateAcesScore(9, 1), Is.is(11));
        assertThat(calculateAcesScore(9, 2), Is.is(12));
        assertThat(calculateAcesScore(9, 3), Is.is(3));
        assertThat(calculateAcesScore(9, 4), Is.is(4));
        assertThat(calculateAcesScore(10, 0), Is.is(0));
        assertThat(calculateAcesScore(10, 1), Is.is(11));
        assertThat(calculateAcesScore(10, 2), Is.is(2));
        assertThat(calculateAcesScore(10, 3), Is.is(3));
        assertThat(calculateAcesScore(10, 4), Is.is(4));


        for (int hardScore = 11; hardScore < 21; hardScore++) {
            assertThat(calculateAcesScore(hardScore, 0), Is.is(0));
            assertThat(calculateAcesScore(hardScore, 1), Is.is(1));
            assertThat(calculateAcesScore(hardScore, 2), Is.is(2));
            assertThat(calculateAcesScore(hardScore, 3), Is.is(3));
            assertThat(calculateAcesScore(hardScore, 4), Is.is(4));
        }
    }
}
