package com.alice.game.blackjack;

import com.alice.game.blackjack.model.Hand;
import com.alice.game.blackjack.model.dto.Card;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static com.alice.game.blackjack.model.dto.CardSuite.*;
import static com.alice.game.blackjack.model.dto.CardValue.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(BlockJUnit4ClassRunner.class)
public class HandTest {

    @Test
    public void testRegularHandScores() {
        Hand actualHand = Hand.init(Card.of(V2, C), Card.of(V2, H));
        assertThat(actualHand.calculateScore(), is(4));
        actualHand = Hand.init(Card.of(V3, S), Card.of(V2, H));
        assertThat(actualHand.calculateScore(), is(5));
        actualHand = Hand.init(Card.of(V4, C), Card.of(V4, D));
        assertThat(actualHand.calculateScore(), is(8));
        actualHand = Hand.init(Card.of(V5, D), Card.of(V4, D));
        assertThat(actualHand.calculateScore(), is(9));
        actualHand = Hand.init(Card.of(V6, C), Card.of(V5, D));
        assertThat(actualHand.calculateScore(), is(11));
        actualHand = Hand.init(Card.of(V7, H), Card.of(V7, D));
        assertThat(actualHand.calculateScore(), is(14));
        actualHand = Hand.init(Card.of(V8, C), Card.of(V7, D));
        assertThat(actualHand.calculateScore(), is(15));
        actualHand.addCard(Card.of(V10, S));
        assertThat(actualHand.calculateScore(), is(25));
        actualHand = Hand.init(Card.of(V9, C), Card.of(V8, C));
        assertThat(actualHand.calculateScore(), is(17));
        actualHand = Hand.init(Card.of(V10, C), Card.of(V9, C));
        assertThat(actualHand.calculateScore(), is(19));
        actualHand = Hand.init(Card.of(JACK, D), Card.of(V10, C));
        assertThat(actualHand.calculateScore(), is(20));
        actualHand = Hand.init(Card.of(QUEEN, H), Card.of(JACK, D));
        assertThat(actualHand.calculateScore(), is(20));
        actualHand = Hand.init(Card.of(KING, C), Card.of(QUEEN, H));
        assertThat(actualHand.calculateScore(), is(20));
        actualHand = Hand.init(Card.of(ACE, S), Card.of(KING, C));
        assertThat(actualHand.calculateScore(), is(21));
    }

    @Test
    public void testAcesBoundaryScores() {
        Hand hand = Hand.init(Card.of(V10, S), Card.of(ACE, S));
        assertThat(hand.calculateScore(), is(21));
        hand.addCard(Card.of(ACE, D));
        assertThat(hand.calculateScore(), is(12));
        hand.addCard(Card.of(ACE, H));
        assertThat(hand.calculateScore(), is(13));
        hand.addCard(Card.of(ACE, C));
        assertThat(hand.calculateScore(), is(14));
        hand = Hand.init(Card.of(V2, S), Card.of(ACE, S));
        assertThat(hand.calculateScore(), is(13));
        hand.addCard(Card.of(ACE, C));
        assertThat(hand.calculateScore(), is(14));
        hand.addCard(Card.of(ACE, D));
        assertThat(hand.calculateScore(), is(15));
        hand.addCard(Card.of(ACE, H));
        assertThat(hand.calculateScore(), is(16));
        hand.addCard(Card.of(V5, H));
        assertThat(hand.calculateScore(), is(21));
        hand.addCard(Card.of(JACK, H));
        assertThat(hand.calculateScore(), is(21));
        hand.addCard(Card.of(V3, H));
        assertThat(hand.calculateScore(), is(24));

    }

}
