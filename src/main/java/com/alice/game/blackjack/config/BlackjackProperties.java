package com.alice.game.blackjack.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("blackjack")
public class BlackjackProperties {
    private int deckCount;
    private int seedCapital;
    private int maxBalance;
    private int bet;

}
