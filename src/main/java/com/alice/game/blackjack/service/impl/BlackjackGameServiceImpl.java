package com.alice.game.blackjack.service.impl;

import com.alice.game.blackjack.service.BlackjackGameService;
import com.alice.game.blackjack.service.BlackjackTableService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Scanner;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Slf4j
@RequiredArgsConstructor
@Service
@Scope(SCOPE_PROTOTYPE)
public class BlackjackGameServiceImpl implements BlackjackGameService {

    private final BlackjackTableService blackjackTableService;

    @Value("${blackjack.max-balance}")
    private int maxBalance;

    public void showMainMenu() {
        int choice = -1;
        do {
            System.out.println("******MAIN MENU******");
            System.out.println("*1 - Start game\t\t*");
            System.out.println("*2 - Show balance\t*");
            System.out.println("*3 - Add balance\t*");
            System.out.println("*4 - Refresh shoe\t*");
            System.out.println("*0 - Exit\t\t\t*");
            System.out.println("*********************");
            Scanner scanner = new Scanner(System.in);
            try {
                choice = scanner.nextInt();
                switch (choice) {
                    case 0 -> System.out.println("Have a nice day!");
                    case 1 -> blackjackTableService.startGame();
                    case 2 -> System.out.println(String.format("Your balance is $%d", blackjackTableService.getUserBalance()));
                    case 3 -> addBalance();
                    case 4 -> blackjackTableService.refreshShoe();
                    default -> System.out.println("Unknown command, please try again.");
                }
            } catch (Exception ex) {
                System.out.println("Invalid input, please try again.");
            }
        } while (choice != 0);
    }

    private void addBalance() {
        System.out.println("Please enter the amount of adding balance:");
        Scanner scanner = new Scanner(System.in);
        try {
            int amount = scanner.nextInt();
            if (amount + blackjackTableService.getUserBalance() >= maxBalance) {
                System.out.println(String.format("Slow your horses! You cannot have much more greater than $%d", maxBalance));
                System.out.println(String.format("Current balance is $%d", blackjackTableService.getUserBalance()));
            } else if (amount >= 0) {
                blackjackTableService.addBalance(amount);
                System.out.println(String.format("Your current balance is $%d", blackjackTableService.getUserBalance()));
            } else {
                System.out.println("Amount could not be negative!");
            }
        } catch (Exception ex) {
            System.out.println("Invalid entered balance!");
        }
    }


}
