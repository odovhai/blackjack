package com.alice.game.blackjack.service.impl;

import com.alice.game.blackjack.config.BlackjackProperties;
import com.alice.game.blackjack.model.Hand;
import com.alice.game.blackjack.model.Shoe;
import com.alice.game.blackjack.model.dto.Card;
import com.alice.game.blackjack.service.BlackjackTableService;
import lombok.Getter;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Service
@Scope(SCOPE_PROTOTYPE)
public class BlackjackTableServiceImpl implements BlackjackTableService {

    private final Shoe shoe;
    private final BlackjackProperties properties;

    @Getter
    private int userBalance;
    private Hand dealer;
    private List<Pair<Hand, Integer>> userHandBets;

    private static final int BLACKJACK_SCORE = 21;

    public BlackjackTableServiceImpl(Shoe shoe, BlackjackProperties properties) {
        this.shoe = shoe;
        this.properties = properties;
    }

    @PostConstruct
    private void init() {
        userBalance = properties.getSeedCapital();
    }

    public void refreshShoe() {
        shoe.refresh();
    }

    public void startGame() {
        if (userBalance - properties.getBet() < 0) {
            System.out.println("Insufficient funds to start new game! Please increase your balance!");
            return;
        }

        System.out.println("Game started.");
        initGame();
        System.out.println("Initial cards are dealt: ");
        System.out.println("Dealer hand:");
        System.out.println(String.format("|%s| CLOSED|", dealer.getCards().get(0)));
        Hand userInitHand = userHandBets.get(0).getLeft();
        if (userInitHand.calculateScore() == BLACKJACK_SCORE) {
            System.out.println("BLACKJACK!");
            if (dealer.calculateScore() == BLACKJACK_SCORE) {
                System.out.println("Dealer also has BLACKJACK! Nobody has won!");
                userBalance += properties.getBet();
            } else {
                int extraWin = 3 * properties.getBet();
                userBalance += extraWin;
                System.out.println(String.format("You have got $%d!", extraWin));
            }
            return;
        }
        processUserActions();

        long inGameUserHands = userHandBets.stream()
                .mapToInt(Pair::getValue)
                .filter(value -> value > 0)
                .count();

        if (inGameUserHands == 0) {
            System.out.println("No active in game user hans. Game Over!");
            return;
        }

        processDealer();
        int dealerScore = dealer.calculateScore();
        System.out.println(String.format("Dealer's score is: %s", dealerScore));

        int totalWin = 0;
        if (dealerScore > BLACKJACK_SCORE) {
            System.out.println("Dealer lost the game!");
            totalWin = 2 * userHandBets.stream()
                    .mapToInt(Pair::getValue)
                    .sum();
        } else {
            for (Pair<Hand, Integer> userHandBet : userHandBets) {
                if (userHandBet.getValue() > 0) {
                    int userScore = userHandBet.getLeft().calculateScore();
                    if (userScore > dealerScore) {
                        totalWin += 2 * userHandBet.getValue();
                    } else if (userScore == dealerScore) {
                        totalWin += userHandBet.getValue();
                    }
                }
            }
        }
        userBalance += totalWin;
        System.out.println(String.format("You have got $%d", totalWin));
        System.out.println("Game ended.");
    }

    public void addBalance(int amount) {
        userBalance += amount;
    }

    private void initGame() {
        userHandBets = new ArrayList<>();
        userHandBets.add(MutablePair.of(Hand.init(shoe.takeCard(), shoe.takeCard()), properties.getBet()));
        userBalance -= properties.getBet();
        System.out.println(String.format("You have spent $%s)", properties.getBet()));
        dealer = Hand.init(shoe.takeCard(), shoe.takeCard());
    }

    private void processUserActions() {
        for (int i = 0; i < userHandBets.size(); i++) {
            System.out.println(String.format("Playing user hand %d:", i + 1));
            Pair<Hand, Integer> handBetPair = userHandBets.get(i);
            Hand userHand = handBetPair.getLeft();
            int choice;
            do {
                System.out.println(userHand);
                Scanner scanner = new Scanner(System.in);
                System.out.println("****CHOSE ACTION*****");
                System.out.println("*1 - Hit\t\t\t*");
                System.out.println("*2 - Stand\t\t\t*");
                System.out.println("*3 - Double Down\t*");
                System.out.println("*4 - Split\t\t\t*");
                System.out.println("*5 - Surrender\t\t*");
                System.out.println("*********************");
                try {
                    choice = scanner.nextInt();
                    if (choice < 1 || choice > 5) {
                        System.out.println("Unknown command, please try again.");
                        continue;
                    }
                    Card userFirstCard = userHand.getCards().get(0);
                    Card userSecondCard = userHand.getCards().get(1);
                    if (choice == 4 && (userHand.getCards().size() > 2 ||
                            userFirstCard.getCardValue() != userSecondCard.getCardValue())) {
                        System.out.println("Split it now allowed! You can split only if you have 2 cards with same car values! Chose another action.");
                        continue;
                    }
                    if (1 == choice) {
                        Card takenCard = shoe.takeCard();
                        System.out.println(String.format("Taken card: %s", takenCard));
                        userHand.addCard(takenCard);
                        System.out.println(userHand);
                        int score = userHand.calculateScore();
                        System.out.println(String.format("Total score: %d", score));
                        if (score > BLACKJACK_SCORE) {
                            System.out.println("You lost!");
                            handBetPair.setValue(0);
                            break;
                        }
                    } else if (4 == choice) {
                        userHand = Hand.init(userFirstCard, shoe.takeCard());
                        userHandBets.add(MutablePair.of(Hand.init(userSecondCard, shoe.takeCard()), properties.getBet()));
                        userBalance -= properties.getBet();
                        System.out.println(String.format("You have spent $%s)", properties.getBet()));
                    } else break;

                } catch (Exception ex) {
                    System.out.println("Invalid entered choice, please try again.");
                }
            } while (true);

            Integer currentBet = handBetPair.getValue();
            if (3 == choice) {
                handBetPair.setValue(2 * currentBet);
                userBalance -= currentBet;
                System.out.println(String.format("You have spent $%s)", currentBet));
                Card takenCard = shoe.takeCard();
                System.out.println(String.format("Taken card: %s", takenCard));
                userHand.addCard(takenCard);
                System.out.println(userHand);
                int score = userHand.calculateScore();
                System.out.println(String.format("Your total score is %d", score));
                if (score > BLACKJACK_SCORE) {
                    System.out.println("You lost!");
                    handBetPair.setValue(0);
                }
            } else if (5 == choice) {
                userBalance += currentBet / 2;
                handBetPair.setValue(0);
            }
        }
    }

    private void processDealer() {
        System.out.println("It's a dealer's turn!");
        System.out.println("Dealers cards are: ");
        System.out.println(dealer);
        TimeUnit time = TimeUnit.SECONDS;
        while (dealer.calculateScore() <= 16) {
            try {
                time.sleep(2);
            } catch (InterruptedException ignored) {
            }
            Card takenCard = shoe.takeCard();
            System.out.println(String.format("Dealer took %s", takenCard));
            dealer.addCard(takenCard);
        }
        System.out.println("Dealer finished! The cards are: ");
        System.out.println(dealer);
    }

}
