package com.alice.game.blackjack.service;

public interface BlackjackTableService {

    void refreshShoe();

    void startGame();

    void addBalance(int amount);

    int getUserBalance();
}
