package com.alice.game.blackjack.model.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(staticName = "of")
public final class Card implements Comparable<Card> {
    public final CardValue cardValue;
    public final CardSuite cardSuite;

    @Override
    public int compareTo(Card o) {
        return Integer.compare(cardValue.getIndex(), o.getCardValue().getIndex());
    }

    @Override
    public String toString() {
        return cardValue + " " + cardSuite;
    }
}
