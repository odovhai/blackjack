package com.alice.game.blackjack.model.dto;

import lombok.Getter;

public enum CardValue {

    V2(2),
    V3(3),
    V4(4),
    V5(5),
    V6(6),
    V7(7),
    V8(8),
    V9(9),
    V10(10),
    JACK(11),
    QUEEN(12),
    KING(13),
    ACE(14);

    CardValue(int index) {
        this.index = index;
    }

    @Getter
    private int index;

}
