package com.alice.game.blackjack.model;


import com.alice.game.blackjack.model.dto.Card;
import com.alice.game.blackjack.util.CardUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Slf4j
@Component
@Scope(SCOPE_PROTOTYPE)
public class Shoe {

    @Value("${blackjack.deck-count}")
    private int deckCount;

    private Queue<Card> cards;

    public Card takeCard() {
        Card takenCard = cards.poll();
        if (null == takenCard) {
            log.info("Shoe is empty!");
            refresh();
            return takeCard();
        }
        return takenCard;
    }

    @PostConstruct
    public void refresh() {
        log.info("Refreshing shoe.");
        cards = new LinkedList<>();
        for (int i = 0; i < deckCount; i++) {
            cards.addAll(CardUtils.createShuffledDeckOfCards());
        }
        Collections.shuffle((LinkedList) cards);
    }

}
