package com.alice.game.blackjack.model.dto;

import lombok.Getter;

public enum CardSuite {

    S("Spades"),
    H("Hearts"),
    D("Diamonds"),
    C("Clubs");

    @Getter
    private String name;

    CardSuite(String name) {
        this.name = name;
    }


}
