package com.alice.game.blackjack.model;

import com.alice.game.blackjack.model.dto.Card;
import com.alice.game.blackjack.model.dto.CardValue;
import com.alice.game.blackjack.util.CardUtils;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hand {
    @Getter
    private List<Card> cards = new ArrayList<>();

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("|");
        cards.forEach(card -> result.append(card).append("| "));
        return result.toString();
    }

    private Hand(Card first, Card second) {
        cards.add(first);
        cards.add(second);
    }

    public static Hand init(Card first, Card second) {
        return new Hand(first, second);
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public int calculateScore() {


        return cards.stream()
                .map(Card::getCardValue)
                .collect(Collectors.teeing(
                        Collectors.summingInt(CardUtils::getNoAceScore),
                        Collectors.summingInt(value -> value == CardValue.ACE ? 1 : 0),
                        (noAcesScore, acesCount) -> noAcesScore + CardUtils.calculateAcesScore(noAcesScore, acesCount))
                );
    }

}
