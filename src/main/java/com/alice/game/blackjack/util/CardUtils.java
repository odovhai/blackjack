package com.alice.game.blackjack.util;

import com.alice.game.blackjack.model.dto.Card;
import com.alice.game.blackjack.model.dto.CardSuite;
import com.alice.game.blackjack.model.dto.CardValue;
import lombok.experimental.UtilityClass;

import java.util.*;
import java.util.stream.Collectors;

@UtilityClass
public class CardUtils {

    public static int getNoAceScore(CardValue cardValue) {
        return switch (cardValue) {
            case V2, V3, V4, V5, V6, V7, V8, V9, V10 -> cardValue.getIndex();
            case JACK, QUEEN, KING -> 10;
            case ACE -> 0;
        };
    }

    public static int calculateAcesScore(int noAcesScore, int acesCount) {
        if (0 == acesCount) {
            return 0;
        }
        return noAcesScore > 11 - acesCount ? acesCount : 11 + acesCount - 1;
    }

    public static List<Card> createShuffledDeckOfCards() {
        List<Card> cards = Arrays.stream(CardValue.values())
                .flatMap(cardValue -> Arrays.stream(CardSuite.values())
                        .map(cardSuite -> Card.of(cardValue, cardSuite)))
                .collect(Collectors.toCollection(LinkedList::new));
        Collections.shuffle(cards);
        return cards;

    }
}
