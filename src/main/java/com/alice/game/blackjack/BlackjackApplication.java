package com.alice.game.blackjack;

import com.alice.game.blackjack.service.BlackjackGameService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class BlackjackApplication implements CommandLineRunner {

    private final BlackjackGameService gameService;

    public static void main(String[] args) {
        SpringApplication.run(BlackjackApplication.class, args);
    }

    @Override
    public void run(String... args) {
        if (args.length == 0) {
            log.warn("Blackjack is not started for playing. Please check command line arguments");
            return;
        }
        String userName = args[0];
        log.info("Blackjack application is started");
        System.out.println(String.format("Hello %s! Welcome to our black jack!", userName));
        gameService.showMainMenu();
        log.info("Blackjack application is ended.");
    }
}
